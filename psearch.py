import json
import sys
from os import listdir

#Define JSON Truth as Pythonic Truth
true = True
false = False

class pSearch:
    def __init__(self, importKey, importValue):
        self.queryPool = self.importJSON()
        if __name__ == "__main__":
            self.searchKey = str(sys.argv[1])
            self.searchValue = str(sys.argv[2])
        else:
            self.searchKey = importKey
            self.searchValue = importValue

    def run(self):
       print(self.search(self.searchKey, self.searchValue, self.queryPool))

              
    def importJSON(self):
        '''Imports all json files, returns array of dicts'''
        data = []
        for index, file in enumerate(listdir('./')):
            if file.endswith('.json'):
                with open(file) as impData:
                    data.append(json.load(impData))
        return data

    def search(self, searchkey, searchval, pool = "!MISSINGDATA"):
        '''Almost-optimised variant of the two searches.
        This will check if the key exists before searching an item.
        Returns an array to be pretty-printed.'''
        if(pool == "!MISSINGDATA"):
            pool = self.queryPool
        if(searchkey == "*"):
            return self.searchThorough(searchval, pool)
        else:
            result = []
            for index, jsonListItem in enumerate(pool):
                potentialIndex = index
                for index, possibleResultObject in enumerate(jsonListItem):
                    potentialResultIndex = index
                    try:
                        if searchkey in possibleResultObject.keys():
                            #print(possibleResultObject[searchkey])
                            if (possibleResultObject[searchkey] == int(searchval)):
                                result.append(pool[potentialIndex][index])
                        else:
                            continue
                        
                    except:
                        continue
                    
            return result

    def searchThorough(self, searchValue, pool = "!MISSINGDATA"):
        '''Primitive variation on search()
        Searches through _everything_.
        Expect this to be slower than search()
        Returns an array of results to be pretty-printed.
        '''
        result = []
        for index, jsonListItem in enumerate(pool):
                potentialIndex = index
                
                for index, possibleResultObject in enumerate(jsonListItem):
                        potentialResultIndex = index
                        
                        if (searchValue in pool[potentialIndex][potentialResultIndex].values()):
                                result.append(pool[potentialIndex][potentialResultIndex])
                                continue
                                
                        else:
                                for possibleResult in possibleResultObject:
                                        query = pool[potentialIndex][potentialResultIndex][possibleResult]
                                        if(type(query) == int):
                                                try:
                                                    if(int(query) == int(searchValue)):
                                                        result.append(pool[potentialIndex][potentialResultIndex])
                                                except ValueError:
                                                    continue
                                                                
                                        if(type(query) == list):
                                                if(searchValue in query):
                                                        result.append(pool[potentialIndex][index])
                                        else:
                                                if(query == searchValue):
                                                        result.append(pool[potentialIndex][index])
        return result


def main():
    search = []
    if(testArgs()):
        search = pSearch(str(sys.argv[1]), str(sys.argv[2]))
        search.run()


def testArgs():
    try:
       temp1 = str(sys.argv[1])
       temp2 = str(sys.argv[2])
       return True
    except IndexError:
        if (prettyArgsError() == False):
            raise
        
def prettyArgsError():
    if __name__ == "__main__":
        print("Proper usage is psearch.py <key> <value>!")
        print("Ex: psearch.py _id 101")
        return True
    else:
        return False


#Application entry point
if __name__ == "__main__":
    main()
