import json
import sys
import os

from multiprocessing import Pool, TimeoutError

#Define JSON Truth as Pythonic Truth
true = True
false = False

def importjson(target):
    '''Imports json and returns as a python array of dicts'''
    with open(target) as importData:
        data = json.load(importData)
    return data

def importJSON():
    '''Imports all json files, returned as an array of dicts'''
    data = []
    i = 0
    for index, file in enumerate(os.listdir('./')):
        if file.endswith('.json'):
            data.append(importjson(file))
    return data

def printHelp():
    '''Prints help to console'''
    print("Run from command line as follows: ")
    print("python3 psearch.py <search term>")
    return None

def search(sTerm, pool):
    '''A naive search function.
    Takes two params, s(earch)Term and pool(of possible results).
    As the simplest introduction to searching, it simply iterates
    over the json as an array of dictionaries, matching full values.
    '''
    result = []
    
    for index, jsonListItem in enumerate(pool):
        potentialIndex = index
        for index, possibleResultObject in enumerate(jsonListItem):
            potentialResultIndex = index
            
            if(sTerm in pool[potentialIndex][potentialResultIndex].values()):
                result.append(pool[potentialIndex]
                              [potentialResultIndex])
            for possibleResult in possibleResultObject:
                query = pool[potentialIndex][potentialResultIndex][possibleResult]
            
                if(type(query) == int):
                    try:
                        if(query == int(sTerm)):
                            result.append(pool[potentialIndex][index])
                    except ValueError as err:
                        continue
                if(type(query) == list):
                    if(sTerm in query):
                        result.append(pool[potentialIndex][index])
                    else:
                        if(query == sTerm):
                            result.append(pool[potentialIndex][index])
    return result
    
def main():
    try:
        searchTerm = str(sys.argv[1])
    except IndexError:
        if __name__ == "__main__":
            #avoid cluttering tests
            print("Please add one search term as an argument;",
                  "ie 'python3 psearch.py Mary'")
        raise
    if (searchTerm == "help"):
        printHelp()
    else:
        searchPool = importJSON()
        print(search(searchTerm, searchPool))
    pass   

if __name__ == "__main__":
    main()
