import unittest
import psearch

class JSONTestCases(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.searchTest = psearch.pSearch("_id", "101")
        self.testData = self.searchTest.importJSON()

    def test_args(self):
        #ensure that the wrong arguments are broken.
        with self.assertRaises(IndexError):
            psearch.main()
        
    def test_json(self):
        #Tests to ensure the imports are what we expect.
        #The system is designed to build an array of arrays of dicts.
        self.assertEqual(type(self.testData),list)
        self.assertEqual(type(self.testData[0]),list)
        self.assertEqual(type(self.testData[0][0]), dict)
        pass

    
    def test_search(self):     
        #Assume search returns an array, get a useful result
        self.assertEqual(type(self.searchTest.search("Mary", self.testData)), list)
        pass
    
    def test_thorough_search(self):
    	#Assume thorough search returns an array, fetches a useful result
    	self.assertEqual(type(self.searchTest.searchThorough("Mary", self.testData)), list)
    	pass
        
if __name__ == '__main__':
    unittest.main()
