# psearch.py

## Requirements:
	- Python3. This _may_ work in python2.7, but I do not have that interpreter installed and have gone into this assuming a modern interpreter.
	- No other library is currently required, as the application relies on the standard library.
		- This was written assuming a linux derivative OS.
		- As such, I cannot guarantee the functionality of the json loader
		  on Windows.


## Usage:
The command line syntax for this application is `psearch.py <key> <value>`
You can, if you desire, 'thorough' search with the `"*" <value>` command.
NOTE: it -does- have to be `"*"`, not simply `*`.

The library's `search()` function returns an array of results (as dicts). One could, with this, process the data in any number of ways.

## Notes, thoughts, etc:
So this was technically my first non-trivial TDD-based appliation, and I feel like the test suite will reflect that. I focused purely on ensuring successful functionality, perhaps to the detriment of edge cases (which I have made an effort to counteract both in tests and code, just not as well as I could have.)

Working with TDD has, however, made me interested to use the paradigm more at at my current work, where working with development and design patterns are not frowned upon but rather not implemented.

In terms of actual developent, I had initially completed the naive "*" version of the code first, then, upon rereeading the spec, realised that I'd misread the task and redeveloped for the <key> <value> search method. While I took the opportunity to refactor a little, I am unsure how much faster one could do this using Python's iterators. Performing the up-front loading of files into memory is a start. One could move to parallelise the process across a number of threads. This last week, I'd started looking into elixir, who's mass-process functions deemed ideal for a task such as this.
